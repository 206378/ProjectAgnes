var React = require('react');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var IndexRoute = ReactRouter.IndexRoute;
var hashHistory = ReactRouter.hashHistory;

var BasePage = require('./components/BasePage.jsx');
var HomePage = require('./components/HomePage.jsx');
var PokemonPage = require('./components/ProductPage.jsx');
var RandomPage = require('./components/RandomPage.jsx');

var Routes = (
    <Router history={hashHistory}>
        <Route path="/" component={BasePage}>
            <IndexRoute component={HomePage}/>
            <Route path="/pokemon/:pokemonId" component={PokemonPage}/>
            <Route path="/random" component={RandomPage}/>
        </Route>
    </Router>
);

module.exports = Routes;