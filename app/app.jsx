var React = require('react');
var ReactDOM = require('react-dom');
var Nav = require('./components/nav/Nav.jsx');
var Routes = require('./Routes.jsx');
var style = require('./styles/app.scss');


ReactDOM.render(Routes,document.getElementById('pokedex'));