var React = require('react');
var Nav = require('./nav/Nav.jsx');

var navLinks = [{title: 'Home',href:'/'},{title: 'Random',href: '/random'}];

var BasePage = React.createClass({
    render: function () {
        return (
            <div>
                <Nav bgColor="white" titleColor="#3097d1"  navData={navLinks}/>
                {this.props.children}
            </div>

        );
    }
});

module.exports = BasePage;