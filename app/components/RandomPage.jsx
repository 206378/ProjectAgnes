var React = require('react');
var ReactRouter = require('react-router');
var Type = require('./Type.jsx');
var Stats = require('./Stats.jsx');

var RandomPage = React.createClass({
    getRandom: function(bottom, top) {
        return Math.floor( Math.random() * ( 1 + top - bottom ) ) + bottom;

    },
    getInitialState: function () {
        return {
            id: this.getRandom(1,811),
            pokemon: null
        }
    },
    componentDidMount: function () {
        var self = this;
        fetch("http://pokeapi.co/api/v2/pokemon/"+this.state.id+"/").then(function(response) {
            return response.json()
        }).then(function(body) {
            self.setState({
                pokemon:body
            });
        });
    },
    render: function () {
        var createType = function (t,index) {
            return <Type key={index + t.type.name} text={t.type.name} />;
        };
        return (
            <div className="thumbnail">
                {this.state.pokemon === null? "": <img className="img-circle " src={this.state.pokemon.sprites.front_default} alt="..." width={140} height={140} />}
                {this.state.pokemon === null? "": <img className="img-circle " src={this.state.pokemon.sprites.back_default} alt="..." width={140} height={140} />}
                <div className="caption text-center">
                    <div className="row">
                        {this.state.pokemon === null? " ":this.state.pokemon.types.map(createType)}
                    </div>
                    Name: {this.state.pokemon === null? " ":this.state.pokemon.name}
                    Id: {this.state.id}
                    Height: {this.state.pokemon === null? " ":this.state.pokemon.height}
                    {this.state.pokemon === null? " ":<Stats stats={this.state.pokemon.stats}/>}
                </div>
            </div>

        );
    }
});


module.exports = RandomPage;