var React = require('react');
var Stat = require('./Stat.jsx');

const Stats = React.createClass({
    render: function () {
        let createTable = function(s,index){
            return <Stat key={index} text={s.base_stat}></Stat>
        };
        return (
            <table className="table table-condensed">
                <thead>
                    <tr>
                        <th>Speed</th>
                        <th>Special Defense</th>
                        <th>Special Attack</th>
                        <th>Defense</th>
                        <th>Attack</th>
                        <th>Hp</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        {this.props.stats.map(createTable)}
                    </tr>
                </tbody>
            </table>
        );
    }
});

module.exports = Stats;