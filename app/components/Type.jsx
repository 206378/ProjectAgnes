var React = require('react');


const Type = React.createClass({
    render: function () {
        return (
            <div className="col-sm-6 col-md-4 col-lg-4">
                <p>{this.props.text}</p>
            </div>
        )
    }
});

module.exports = Type;