var React = require('react');
var ListItem = require('./ListItem.jsx');

var List = React.createClass({
    getInitialState: function () {
        return {
            pokemons: []
        };
    },
    render: function () {

            var createItem = function (p,index) {
             return <ListItem key={index + p.name} pokemon={{id:index+1,name:p.name}} />;
            };

            return (
                <div className="row">
                    {this.props.pokemons.map(createItem)}
                </div>
            )
    }
});

module.exports = List;