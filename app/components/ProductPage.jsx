var React = require('react');
var Type = require('./Type.jsx');
var Stats = require('./Stats.jsx');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;


var PokemonPage = React.createClass({
    getInitialState: function () {
        return {
            id: this.props.params.pokemonId,
            pokemon: null
        }
    },
    componentDidMount: function () {
      var self = this;
      fetch("http://pokeapi.co/api/v2/pokemon/"+this.props.params.pokemonId+"/").then(function(response) {
           return response.json()
      }).then(function(body) {
           self.setState({
               pokemon:body
           });
      });
    },
    componentWillReceiveProps: function (nextProps) {
        var self = this;
        fetch("http://pokeapi.co/api/v2/pokemon/"+nextProps.params.pokemonId+"/").then(function(response) {
            return response.json()
        }).then(function(body) {
            self.setState({
                id:nextProps.params.pokemonId,
                pokemon:body
            });
        });
    },
    render: function () {
        var createType = function (t,index) {
            return <Type key={index + t.type.name} text={t.type.name} />;
        };
        return (
            <div className="thumbnail">
                    {this.state.pokemon === null? "": <img className="img-circle " src={this.state.pokemon.sprites.front_default} alt="..." width={140} height={140} />}
                    {this.state.pokemon === null? "": <img className="img-circle " src={this.state.pokemon.sprites.back_default} alt="..." width={140} height={140} />}
                <div className="caption text-center">
                    <div className="row">
                        {this.state.pokemon === null? " ":this.state.pokemon.types.map(createType)}
                    </div>
                    Name: {this.state.pokemon === null? " ":this.state.pokemon.name}
                    Id: {this.state.id}
                    {this.state.pokemon === null? " ":<Stats stats={this.state.pokemon.stats}/>}
                </div>
                {this.props.params.pokemonId < 719 ? <Link to={"pokemon/"+(Number(this.state.id)+1)}>Next</Link>: ""}
                {this.props.params.pokemonId > 1? <Link to={"pokemon/"+(Number(this.state.id)-1)}>Preview</Link>: ""}
            </div>

        );
    }
});

module.exports = PokemonPage;