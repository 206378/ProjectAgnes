var React = require('react');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;
var List = require('./List.jsx');
var P = require('../../data.json');



var HomePage = React.createClass({
    getInitialState : function () {
        return {
            pokemon:P.pokemons,

        };
    },
    render: function () {
        return (
            <div className="container">
                <h1>Pokemons: 718 </h1>
                {this.state.pokemon === null ?  "":<List pokemons={this.state.pokemon}></List>}
            </div>
        );
    }
});

module.exports = HomePage;