var React = require('react');
var ReactRouter = require('react-router');
var Type = require('./Type.jsx');
var Link = ReactRouter.Link;


var ListItem = React.createClass({
    getInitialState: function () {
        return {
            pokemon: null
        };
    },
    componentDidMount: function () {
        var self = this;
        fetch("http://pokeapi.co/api/v2/pokemon/"+this.props.pokemon.id+"/").then(function(response) {
            return response.json()
        }).then(function(body) {
            self.setState({
                pokemon:body
            });
        });
    },
    render: function () {
        return (
            <div className="col-sm-6 col-md-4 col-lg-4">
                <div className="thumbnail" width="350" height="350">
                    <img className="img-circle" src={"Pokemon/"+this.props.pokemon.id+".png"} alt="..." />
                        <div className="caption text-center">
                            <Link to={"pokemon/"+this.props.pokemon.id}>{this.props.pokemon.name}</Link>
                        </div>
                </div>
            </div>
        );
    }
});

module.exports = ListItem;
