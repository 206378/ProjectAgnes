var React = require('react');

const Stat = React.createClass({
    render: function () {
        return (
            <td>{this.props.text}</td>
        );
    }
});

module.exports = Stat;